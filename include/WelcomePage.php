<?php
include_once '../config.php';
$config = new Config();
?>
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Короткие ссылки</title>

    <!-- Styles -->
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/main.css">
</head>
<body>
<div class="card">
    <div class="card-body">
        <h5 class="card-title">Короткие ссылки</h5>
        <form id="url-form">
            <label for="original_url">Оригинальная ссылка</label>
            <input name="original_url" id='original_url' type='text' class="form-control" value="<?= $config->def_original_url?>" onclick="return false;"> <br>
            <button id="get-url" type="button" class="btn btn-primary" onclick="getShortUrl()">Получить</button>
        </form>
    </div>
</div>

<div class="card">
    <div class="card-body">
        <table id="urls-table" class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Ориг. ссылка</th>
                <th scope="col">Короткая ссылка</th>
                <th scope="col"></th>
                <th scope="col">Создано</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</div>

</body>

<!-- Scripts -->
<script src="/js/jquery.min.js"></script>
<script src="/js/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/clipboard.min.js"></script>
<script src="/js/main.js"></script>
</html>
