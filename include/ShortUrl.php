<?php

require_once '../config.php';
require_once '../db/DataBase.php';

class ShortUrl
{
    protected $allowedChars;
    protected $db;
    protected $host;

    /**
     * Перевод числа в систему счисления из символов $allowedChars
     * @param int $i
     * @return string
     */
    public function intToStr(int $i) {
        $chars = $this->allowedChars;
        $n = strlen($chars);
        $result = '';

        while ($i != 0) {
            $c = $i % $n;
            $result .= $chars[$c];
            $i = intdiv($i, $n);
        }

        return $result;
    }

    function __construct($allowedChars = null) {
        $this->db = new DataBase();
        $this->allowedChars = $this->db->getOption('allowedChars');
        $this->host = (new Config())->host;
    }

    /**
     * Поиск в БД короткой ссылки для исходной или генерация и вставка короткой ссылки
     * @param string $originalUrl
     * @return string json
     */
    public function jsonShortUrl(string $originalUrl)
    {
        if(!$originalUrl)
            return json_encode([
                'state' => 'error',
                'message' => 'Оригинальная ссылка не передана.',
            ]);

        // проверяем наличие схемы http(s)
        $scheme = parse_url($originalUrl, PHP_URL_SCHEME);
        if(!$scheme) $originalUrl = 'http://'.$originalUrl;

        $sql = "SELECT * FROM `short_urls` WHERE `original_url` = ?;";
        $url = $this->db->executeSql($sql, [$originalUrl])->fetch();

        if(!$url) { // оригинальной ссылки нет БД
            $counter = $this->db->getOption('urlsCounter');

            $shortPath = $this->intToStr($counter);
            $counter++;
            $this->db->setOption('urlsCounter', $counter);

            $sql = "INSERT INTO `short_urls` (`original_url`, `short_path`) VALUES (?, ?);";
            $this->db->executeSql($sql, [$originalUrl, $shortPath]);

            return json_encode([
                'state' => 'success',
                'original_url' => $originalUrl,
                'short_url' => $this->host.$shortPath,
                'created_at' => date('Y-m-d H:i:s'),
            ]);
        }

        return json_encode([
            'state' => 'success',
            'original_url' => $url['original_url'],
            'short_url' => $this->host.$url['short_path'],
            'created_at' => $url['created_at'],
        ]);
    }
    /**
     * @param string $shortUrl
     * @return string original url
     */
    public function originalUrl(string $shortUrl)
    {
        mb_internal_encoding("UTF-8");
        $shortUrl = mb_substr($shortUrl, 1);

        $sql = "SELECT `original_url` FROM `short_urls` WHERE `short_path` = ?;";
        $originalUrl = $this->db->executeSql($sql, [$shortUrl])->fetchColumn();

        return $originalUrl;
    }
}