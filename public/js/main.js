$(document).ready(function(){
    $('#url-form').submit(false);
});

function getShortUrl() {

    $.get(
        "/",
        $('#url-form').serialize(),
        function(data) {
            if(data.state === 'success') {
                $('#urls-table tbody').append(
                    '<tr>' +
                    '<td><a target="_blank" href="' + data.original_url + '">' + data.original_url + '</a></td>' +
                    '<td><a target="_blank" href="' + data.short_url + '">' + data.short_url + '</a></td>' +
                    '<td><button type="button" class="btn btn-primary" title="Копировать" data-clipboard-text="'+
                    data.short_url +'">⎘</button></td>' +
                    '<td>' + data.created_at + '</td>' +
                    '</tr>'
                );
                new ClipboardJS('#urls-table .btn');
            }

            else alert(data.message);
        },
        "json")
        .fail(function() {
            alert( "Ошибка при обращении к серверу." );
        });
}