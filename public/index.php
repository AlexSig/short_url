<?php

require_once '../include/ShortUrl.php';

$shortUrl = new ShortUrl();
$requestUrl = $_SERVER['REDIRECT_URL'];

// пустой url - главная страница
if($requestUrl == '') {
    $originalUrl = $_GET['original_url'];
    if(isset($originalUrl)) {
        header('Content-Type: application/json');
        echo $shortUrl->jsonShortUrl($originalUrl);
    }
    else {
        include '../include/WelcomePage.php';
    }
}
else { // короткий или неправильный адрес
    $originalUrl = $shortUrl->originalUrl($requestUrl);
    if($originalUrl) {
        header("Location: $originalUrl", true, 303);
    }
    else {
        header($_SERVER["SERVER_PROTOCOL"]." 404 Not Found", true, 404);
        echo "Страница не найдена.";
    }
}

?>