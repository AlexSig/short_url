## Короткие url

Демо [http://short-url.x-sites.ru/](http://short-url.x-sites.ru/).

Требования к серверу:

    PHP >= 7.2.0
    mod_rewrite
    MySQL 5.7+
 
 Порядок развертывания:

```sh
$ git clone git@bitbucket.org:AlexSig/short_url.git
$ cd short_url
$ cp config.php.example config.php
```

Создаем БД в MySQL и прописываем настройки в config.php:
```php
public $host = 'http://short.url/';
public $db_host = '127.0.0.1';
public $db_database = 'short_url';
public $db_username = 'homestead';
public $db_password = 'secret';

```

Возможно, `$db_host` надо установить в "localhost".

Миграция БД:
```sh
$ cd db
$ php migration.php 
```

Индексный файл находится в папке `public`.