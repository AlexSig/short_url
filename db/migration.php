<?php

require_once 'DataBase.php';
require_once '../config.php';

echo "Подключение к БД...".PHP_EOL;

$config = new Config();
$db = new DataBase();

echo "Создание таблиц...".PHP_EOL;

$sql = <<<"SQL_SCRIPT"

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE SCHEMA IF NOT EXISTS `$config->db_database` DEFAULT CHARACTER SET $config->db_charset ;
USE `$config->db_database` ;

DROP TABLE IF EXISTS `short_urls` ;

CREATE TABLE IF NOT EXISTS `short_urls` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `original_url` VARCHAR(191) NOT NULL,
  `short_path` VARCHAR(10) NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `short_path_UNIQUE` (`short_path` ASC),
  UNIQUE INDEX `original_url_UNIQUE` (`original_url` ASC))
  ENGINE = InnoDB
  DEFAULT CHARACTER SET = utf8mb4
  COLLATE = utf8mb4_bin;


DROP TABLE IF EXISTS `options` ;

CREATE TABLE IF NOT EXISTS `options` (
  `id` INT UNSIGNED NOT NULL AUTO_INCREMENT,
  `key` VARCHAR(45) NOT NULL,
  `val` VARCHAR(255) NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `key_UNIQUE` (`key` ASC))
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


SQL_SCRIPT;

$db->executeSql($sql);

echo "Вставка данных...".PHP_EOL;

$sql = <<<"SQL_SCRIPT"
START TRANSACTION;
USE `$config->db_database`;
INSERT INTO `options` (`id`, `key`, `val`) VALUES (DEFAULT, ?, ?);

COMMIT;

SQL_SCRIPT;

$allowedChars = $config->allowedChars;
if($config->shuffleChars)
    $allowedChars = str_shuffle($allowedChars);

$db->executeSql($sql, ['allowedChars', $allowedChars]);
$db->executeSql($sql, ['urlsCounter', $config->urlsCounter]);

echo "Миграция завершена.".PHP_EOL;
