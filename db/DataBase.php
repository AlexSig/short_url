<?php

require_once '../config.php';

class DataBase
{
    protected $pdo;

    function __construct() {
        $this->connect();
    }

    /**
     * @return PDO
     */
    public function connect () {
        $config = new Config();

        $dsn = "mysql:host=$config->db_host;dbname=$config->db_database;charset=$config->db_charset";
        $opt = [
            PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
        ];

        $this->pdo = new PDO($dsn, $config->db_username, $config->db_password, $opt);

        return $this->pdo;
    }

    /**
     * @param string $sql
     * @param array $agrs
     * @return PDOStatement
     */
    public function executeSql(string $sql, $agrs = [])
    {
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute($agrs);
        return $stmt;
    }

    /**
     * @param string $key
     * @return string
     */
    public function getOption(string $key)
    {
        $sql = "SELECT `val` FROM `options` WHERE `key` = ?;";
        return $this->executeSql($sql, [$key])->fetchColumn();
    }

    public function setOption(string $key, $val)
    {
        $sql = "UPDATE `options` SET `val` = ? WHERE `key` = ? LIMIT 1;";
        return $this->executeSql($sql, [$val, $key]);
    }

}